const Encore = require('@symfony/webpack-encore')
const path = require('path')

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev')
}

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .addEntry('app', './assets/app.js')
    .addStyleEntry('tailwind', './assets/css/app.css')
    .splitEntryChunks()
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .disableSingleRuntimeChunk()
    .configureBabel(() => {}, {
        useBuiltIns: 'usage',
        corejs: 3
    })
    .enableSassLoader()
    .enablePostCssLoader((options) => {
        options.postcssOptions = {
            config: './postcss.config.js'
        }
    })

    .enableVueLoader()
    .addAliases({
        vue$: 'vue/dist/vue.runtime.esm-browser.js',
        '@': path.resolve('./assets')
    })
module.exports = Encore.getWebpackConfig()
