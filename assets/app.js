import { createApp, h } from 'vue';
import { createInertiaApp, Link, Head } from '@inertiajs/inertia-vue3';
import { InertiaProgress } from '@inertiajs/progress';
//import Layout from '@/Pages/Layouts/Background';
import { dragscrollNext } from "vue-dragscroll";
import './styles/app.scss'


createInertiaApp({
    title: title => title,
    resolve: async name => {
        let page = (await import(`./Pages/${name}`)).default;

        /*if(page.layout === undefined)
            page.layout = Layout;*/

        return page;
    },
    setup({ el, app, props, plugin }) {
        return createApp({ render: () => h(app, props) })
            .use(plugin)
            .component('Link', Link)
            .component('Head', Head)
            .directive('dragscroll', dragscrollNext)
            .mixin({ methods: { route: route } })
            .mount(el);
    },
});

InertiaProgress.init({
    color: '#BA002A',
    showSpinner: true
})
