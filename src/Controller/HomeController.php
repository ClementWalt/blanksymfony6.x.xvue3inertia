<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Rompetomp\InertiaBundle\Service\InertiaInterface;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(InertiaInterface $inertia)
    {
        return $inertia->render('Home',[
            'title' => "Inertia Vue3 Symfony",
        ]);
    }
}
