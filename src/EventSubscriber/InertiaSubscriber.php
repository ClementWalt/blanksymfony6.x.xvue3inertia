<?php

namespace App\EventSubscriber;

use Rompetomp\InertiaBundle\Service\InertiaInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

class InertiaSubscriber implements EventSubscriberInterface
{
    private Security $security;
    /** @var InertiaInterface */
    protected InertiaInterface $inertia;

    /**
     * AppSubscriber constructor.
     *
     * @param InertiaInterface $inertia
     */
    public function __construct(InertiaInterface $inertia, Security $security)
    {
        $this->inertia = $inertia;
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'onControllerEvent',
        ];
    }

    public function onControllerEvent($event)
    {
        $user = $this->security->getUser();
        if($user){
            $this->inertia->share(
                'User',[
                    "id" => $user->getId(),
                    "username" => $user->getUserIdentifier(),
                    "role" => $user->getRoles()
                ]
            );
        }

    }
}