const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  darkMode: 'class',
  content: [
    './templates/**/*.html.twig',
    './assets/Pages/**/*.vue',
  ],

  theme: {
    extend: {
      fontFamily: {
        sans: ['Nunito', ...defaultTheme.fontFamily.sans],
      },
      colors: {
        'cnam' : '#BA002A',
        'dark' : '#141414',
        'lightDark' : '#373737',
        'grayBlue' : '#F6F5F8',
      }
    },
  },
  variants: {
    extend: {},
  },

  plugins: [
    require('@tailwindcss/forms'),
  ],
};
